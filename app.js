const Express = require("express");
const BodyParser = require("body-parser");

var port = require('./config/constants').port
var app = Express();

app.set("json spaces", 2);

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
  res.setHeader('X-Total-Count', 0);
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  res.setHeader('Access-Control-Allow-Headers', '*');
  next();
});

exports.app = app;

const connectDB = require('./config/connectDB');

var callback = function () {
  console.log("Connect db success");
  exports.dbo = connectDB.dbo;
  require("./functionality/user");
  require("./functionality/exam");
  require("./functionality/subject");
  require("./functionality/thematic");
  require("./functionality/question");
  require("./functionality/SystemRoom");
  require("./functionality/history");
  require("./functionality/authenticate");
  require("./functionality/refreshToken");
  app.listen(port, () => {
    console.log("Connect port 4000");
  });
}

connectDB.initConnection(callback);
