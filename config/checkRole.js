var exports = require('..');
var url = require('url');
var getPermissionByRole = require('../functionality/authorization/getPermissionByRole').getPermissionByRole;

const checkRole = async (req, res, next) => {
    return next()
    var pathname = url.parse(req.url).pathname;
    var role = req.user.role;
    var index = pathname.indexOf('/', 1);
    var permission;
    if (index === -1) {
        permission = pathname.substring(1);
    } else {
        permission = pathname.substring(1, index);
    }
    const permissions = await getPermissionByRole(role);
    if (permissions.indexOf(permission) !== -1) {
        next();
    } else {
        res.status(401).send("You don't have permission !");
    }
};

module.exports = {
    checkRole: checkRole
}