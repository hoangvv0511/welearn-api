module.exports = Object.freeze({
    urlConnectDB: "mongodb+srv://binh:mongodb@cluster0-ydsrk.azure.mongodb.net/test?retryWrites=true&w=majority",
    secretOrKey: "binhvntg",
    lifeToken: 24 * 60 * 60, // thời gian tồn tại token : 24 giờ
    port: process.env.PORT || 4000,
    lifeSystemRoom: 3 * 24 * 60 * 60 * 1000 // thời gian phòng thi hệ thống : 3 ngày (milisecond)
});