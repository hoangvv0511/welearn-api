const jwt = require('jsonwebtoken');
const secretOrKey = require('./constants').secretOrKey;

const checkToken = (req, res, next) => {
    return next()
    try {
        return next()
        var token = req.headers.authorization;

        if (token !== undefined) {
            if (token.startsWith('Bearer ')) {
                // Remove Bearer from string
                token = token.slice(7, token.length);
            }
        }

        // Xác thực token
        jwt.verify(token, secretOrKey, (err, payload) => {
            if (payload) {
                req.user = payload;
                next();
            } else {
                // Nếu token tồn tại nhưng không hợp lệ, server sẽ response status code 401 với msg bên dưới
                res.status(401).send('Unauthorized');
            }
        })
    } catch (err) {
        console.log(err);
        // Nếu không có token ở header, server sẽ response status code 401 với msg bên dưới        
        res.status(401).send('No token provided');
    }
};

module.exports = {
    checkToken: checkToken
}