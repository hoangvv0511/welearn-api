var getRequest = (request) => {
    var _end = parseInt(request.query._end);
    var _order = request.query._order;
    var _sort = request.query._sort;
    var _start = parseInt(request.query._start);
    var sort = 1;
    if (!Number.isInteger(_end)) {
        _end = 0;
    }
    if (!Number.isInteger(_start)) {
        _start = 0;
    }
    if (_order === "DESC") {
        sort = -1;
    }
    var querySort = {_id : 1};
    if (_sort !== undefined) {
        querySort = { [_sort]: sort };
    }
    const myRequest = {
        sort: querySort,
        skip: _start,
        limit: _end - _start
    }
    return myRequest;
}

module.exports = {
    getRequest: getRequest
};