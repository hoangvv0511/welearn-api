const MongoClient = require("mongodb").MongoClient;
var url = require('./constants').urlConnectDB;

exports.initConnection = function initConnection(callback) {
    MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
        if (err) throw err;
        try {
            //var dbo = db.db("Test-WeLearnAPI");
            var dbo = db.db("WeLearnAPI");
            exports.dbo = dbo;
        }
        finally {
            callback();
        }
    });
};




