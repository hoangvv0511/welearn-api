const protectedRoute = (req, res, next) => {
    return next()
    // Nếu req.user tồn tại nghĩa là token cũng tồn tại
    return next();
    if (req.user) {
        return next();
    }

    // Ngược lại server sẽ response status code 401 với msg bên dưới 
    res.status(401).send('Unauthorized');
}

module.exports = {
    protectedRoute: protectedRoute
}