var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var getRequest = require('../../config/getRequest').getRequest;

exports.app.get("/history", checkToken, protectedRoute, checkRole, async (request, response) => {
    var historyCollection = await exports.dbo.collection("History");
    queryRequest = getRequest(request);
    var user_id = request.query.user_id;
    var exam_id = parseInt(request.query.exam_id);
    var dateFrom = Date.parse(request.query.dateFrom);
    var dateTo = Date.parse(request.query.dateTo);
    var queryUser = {};
    var queryExam = {};
    var queryDate = {};
    if (user_id !== undefined) {
        queryUser = { user_id: user_id };
    }
    if (Number.isInteger(exam_id)) {
        queryExam = { exam_id: exam_id };
    }
    if (Number.isInteger(dateFrom) && Number.isInteger(dateTo)) {
        queryDate = {
            $and:
                [
                    { start_time: { $gte: dateFrom } },
                    { finish_time: { $lte: dateTo } }
                ]
        }
    }
    var match = {
        $and:
            [
                queryUser,
                queryExam,
                queryDate
            ]
    };
    var query = [
        {
            $lookup:
            {
                from: 'Exam',
                localField: 'exam_id',
                foreignField: 'id',
                as: 'history_exam'
            }
        },
        {
            $unwind: {
                path: "$history_exam",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $match: match
        }
    ]
    try {
        var count = await historyCollection.aggregate(query).toArray();
        var historys = [];
        var project = { 
            _id: 0,
            "history_exam._id": 0,
            "history_exam.id" : 0,
            "history_exam.question": 0,
            "history_exam.is_online": 0,
            "history_exam.start_id": 0,
            "history_exam.count": 0,
            "history_exam.tested_user": 0,
            "history_exam.rated_user": 0,
            "history_exam.star": 0,
            "history_exam.answer": 0
        };
        if (queryRequest.skip === 0 && queryRequest.limit === 0) {
            historys = await historyCollection.aggregate(query)
                .project(project)
                .toArray();
        } else {
            historys = await historyCollection.aggregate(query)
                .skip(queryRequest.skip)
                .limit(queryRequest.limit)
                .sort(queryRequest.sort)
                .project(project)
                .toArray();
        }
        response.setHeader('X-Total-Count', count.length);
        historys.forEach((element, index) => {
            var STT = { STT: index + queryRequest.skip + 1 };
            Object.assign(element, STT);
        });
        response.status(200).json(historys);
    } catch (error) {
        response.sendStatus(error);
        console.log("get history error : " + error);
    }
});