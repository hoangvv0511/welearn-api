var exports = require('../../app');

exports.app.get("/getTopUser", async (request, response) => {
    var historyCollection = await exports.dbo.collection("History");
    var exam_id = parseInt(request.query.exam_id);
    var exam_time = parseInt(request.query.exam_time)*60*1000; // minutes -> timestamp  
    var start_time = parseInt(request.query.start_time);
    var match = {
        $and:
            [
                { start_time: { $gte: start_time } },
                { start_time: { $lte: start_time + exam_time } },
                { exam_id: exam_id }
            ]
    }
    var query = [
        {
            $lookup:
            {
                from: 'User',
                localField: 'user_id',
                foreignField: 'id',
                as: 'history_user'
            }
        },
        {
            $unwind: {
                path: "$history_user",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $match: match
        }
    ]
    var sort = {
        point : -1,
        finish_time : 1
    }
    try {
        var project = {
            _id: 0,
            "history_user._id": 0,
        }
        var historys = await historyCollection.aggregate(query).project(project).sort(sort).limit(50).toArray();
        response.status(200).json(historys);
    } catch (error) {
        response.sendStatus(error);
        console.log("get Top User error : " + error);
    }
});