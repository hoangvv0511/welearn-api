var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var getRequest = require('../../config/getRequest').getRequest;

exports.app.post("/history", checkToken, protectedRoute, checkRole, async (request, response) => {
    var historyCollection = await exports.dbo.collection("History");
    var examCollection = await exports.dbo.collection("Exam");
    queryRequest = getRequest(request);
    var point = parseFloat(request.body.point);
    var start_time = parseFloat(request.body.start_time);
    var finish_time = parseFloat(request.body.finish_time);
    var user_id = request.body.user_id;
    var exam_id = parseInt(request.body.exam_id);
    var queryExam = { id: exam_id };
    try {
        var historys = await historyCollection.find().sort({ id: -1 }).toArray();
        var count = 0;
        if (historys.length !== 0) {
            count = parseInt(historys[0].id);
        }
        newHistory = {
            id: count + 1,
            point: point,
            start_time: start_time,
            finish_time: finish_time,
            user_id: user_id,
            exam_id: exam_id
        };
        await historyCollection.insertOne(newHistory);
        var exam = await examCollection.findOne(queryExam);
        var tested_user = exam.tested_user;
        if (tested_user.indexOf(user_id) === -1) {
            tested_user.push(user_id);
        }
        var newValueExam = {
            $set:
            {
                count: exam.count + 1,
                tested_user: tested_user
            }
        };
        await examCollection.updateOne(queryExam, newValueExam);
        response.status(201).json(newHistory);
    } catch (error) {
        response.sendStatus(error);
        console.log("post history error : " + error);
    }
});