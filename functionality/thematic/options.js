var exports = require('../..');

exports.app.options("/thematic/:id", (request, response) => {
    response.sendStatus(204);
});

exports.app.options("/thematic", (request, response) => {
    response.sendStatus(204);
});