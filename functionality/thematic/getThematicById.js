var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.get("/thematic/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var thematicCollection = await exports.dbo.collection("Thematic");
    var query = { id: request.params.id };
    try {
        const thematic = await thematicCollection.findOne(query);
        if (thematic === null) {
            response.setHeader('X-Total-Count', 0);
            response.status(404).json({});
        } else {
            response.setHeader('X-Total-Count', 1);
            response.status(200).json(thematic);
        }
    } catch (error) {
        response.sendStatus(error);
        console.log("get thematic/id error : " + error);
    }
});