var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.delete("/thematic/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var thematicCollection = await exports.dbo.collection("Thematic");
    var queryDelete = { id: request.params.id };
    try {
        await thematicCollection.deleteOne(queryDelete);
        response.status(200).json({});
    } catch (error) {
        response.sendStatus(error);
        console.log("delete thematic/id error : " + error);
    }
});