require('./getAllThematics');
require('./getThematicById');
require('./options');
require('./createThematic');
require('./deleteThematicById');
require('./editThematicById');