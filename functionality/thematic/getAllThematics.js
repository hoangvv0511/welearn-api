var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var getRequest = require('../../config/getRequest').getRequest;

exports.app.get("/thematic", checkToken, protectedRoute, checkRole, async (request, response) => {
    var thematicCollection = await exports.dbo.collection("Thematic");
    var examCollection = await exports.dbo.collection("Exam");
    queryRequest = getRequest(request);
    var name = request.query.name;
    var subject_id = request.query.subject_id;
    var id = request.query.id;
    var exam_id = parseInt(request.query.exam_id);
    var queryName = {};
    var querySubjectId = {};
    if (name !== undefined) {
        queryName = { name: { $regex: name, $options: 'i' } };
    }
    if (subject_id !== undefined) {
        querySubjectId = { subject_id: subject_id };
    }
    var query = {
        $and: [
            queryName,
            querySubjectId
        ]
    }
    try {
        if (id !== undefined) {
            if (!Array.isArray(id)) {
                var queryThematicById = { id: id };
                var thematics = await thematicCollection.findOne(queryThematicById);
                subject_id = thematics.subject_id;
                query = { subject_id: subject_id };
            } else {
                var queryIds = [];
                id.forEach((element) => {
                    queryIds.push({ id: element });
                });
                var queryThematicByIds = { $or: queryIds };
                var thematics = await thematicCollection.find(queryThematicByIds).toArray();
                var querySubjects = [];
                thematics.forEach((element) => {
                    querySubjects.push({ subject_id: element.subject_id });
                });
                query = { $or: querySubjects };
            }
            var thematics = await thematicCollection.find(query)
                .sort(queryRequest.sort)
                .project({ _id: 0 })
                .toArray();
            response.status(200).json(thematics);
        } else {
            if (Number.isInteger(exam_id)) {
                var exam = await examCollection.findOne({ id: exam_id });
                var subject_id = exam.subject_id;
                query = { subject_id: subject_id };
            }
            var count = await thematicCollection.find(query).toArray();
            var thematics = await thematicCollection.find(query)
                .skip(queryRequest.skip)
                .limit(queryRequest.limit)
                .sort(queryRequest.sort)
                .project({ _id: 0 })
                .toArray();
            thematics.forEach((element, index) => {
                var STT = { STT: index + queryRequest.skip + 1 };
                Object.assign(element, STT);
            });
            response.setHeader('X-Total-Count', count.length);
            response.status(200).json(thematics);
        }
    } catch (error) {
        response.sendStatus(error);
        console.log("get thematic error : " + error);
    }
});