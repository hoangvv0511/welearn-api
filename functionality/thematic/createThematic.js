var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var checkRole = require('../../config/checkRole').checkRole;
var checkIdThematic = require('./lib').checkIdThematic;

exports.app.post("/thematic", checkToken, protectedRoute, checkRole, async (request, response) => {
    var thematicCollection = await exports.dbo.collection("Thematic");
    var id = request.body.id;
    if(await checkIdThematic(id)) {
        response.statusMessage = "ra.notification.idThematicExists";
        response.status(400).end();
        return;
    }
    try {
        newThematic = {
            id: id,
            name: request.body.name,
            subject_id: request.body.subject_id
        };
        await thematicCollection.insertOne(newThematic);
        response.status(201).json(newThematic);
    } catch (error) {
        response.sendStatus(error);
        console.log("post thematic error : " + error);
    }
});