var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var checkIdThematic = require('./lib').checkIdThematic;

exports.app.put("/thematic/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var thematicCollection = await exports.dbo.collection("Thematic");
    var id = request.params.id;
    var query = { id: id };
    var value = {
        id: id,
        name: request.body.name,
        subject_id: request.body.subject_id,
    };
    var newValue = { $set: value };
    const thematic = await thematicCollection.findOne(query);
    if (thematic === null) {
        response.sendStatus(404);
    } else {
        try {
            await thematicCollection.updateOne(query, newValue);
            response.status(200).json(value);
        } catch (error) {
            response.sendStatus(error);
            console.log("put thematic/id error : " + error);
        }
    }
});