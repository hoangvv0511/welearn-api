var exports = require('../../app');

var checkIdThematic = async (id) => {
    var thematicCollection = await exports.dbo.collection("Thematic");
    var query = { id: id };
    var thematic = await thematicCollection.findOne(query);
    return (thematic !== null);
}

module.exports = {
    checkIdThematic: checkIdThematic
}