var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var getRequest = require('../../config/getRequest').getRequest;

exports.app.post("/addStar", checkToken, protectedRoute, checkRole, async (request, response) => {
    var examCollection = await exports.dbo.collection("Exam");
    queryRequest = getRequest(request);
    var user_id = request.body.user_id;
    var id = parseInt(request.body.exam_id);
    var star = parseInt(request.body.star);
    var query = { id: id }
    try {
        var exam = await examCollection.findOne(query);
        var rated_user = exam.rated_user;
        var starExam = exam.star;
        var newExam = {};
        if (rated_user.indexOf(user_id) === -1) {
            rated_user.push(user_id);
            newExam = {
                $set: {
                    rated_user: rated_user,
                    star: starExam + star
                }
            };
            await examCollection.updateOne(query, newExam);
        }
        response.status(200).json(newExam);
    } catch (error) {
        response.sendStatus(error);
        console.log("post add star error : " + error);
    }
});