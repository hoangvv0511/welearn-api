var exports = require('../..');

exports.app.options("/exam/:id", (request, response) => {
    response.sendStatus(204);
});

exports.app.options("/exam", (request, response) => {
    response.sendStatus(204);
});