require('./getAllExams');
require('./getExamById');
require('./options');
require('./createExam');
require('./deleteExamById');
require('./editExamById');
require('./addStarExam');
require('./resetExam');