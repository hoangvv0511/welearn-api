var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var getRequest = require('../../config/getRequest').getRequest;

exports.app.get("/exam", checkToken, protectedRoute, checkRole, async (request, response) => {
    var examCollection = await exports.dbo.collection("Exam");
    var systemRoomCollection = await exports.dbo.collection("SystemRoom");
    var questionCollection = await exports.dbo.collection("Question");
    queryRequest = getRequest(request);
    var name = request.query.name;
    var subject_id = request.query.subject_id;
    var is_onlineString = request.query.is_online;
    var is_existsString = request.query.is_exists;
    var year = parseInt(request.query.year);
    var user_id = request.query.user_id;
    var is_online = false;
    if (is_onlineString === "true") {
        is_online = true
    }
    if (is_onlineString === undefined) {
        is_online = undefined
    }
    var is_testedString = request.query.is_tested;
    var is_tested = false;
    if (is_testedString === "true") {
        is_tested = true
    }
    if (is_testedString === undefined) {
        is_tested = undefined
    }
    var is_exists = false;
    if (is_existsString === "true") {
        is_exists = true
    }
    if (is_existsString === undefined) {
        is_exists = undefined
    }
    var queryName = {};
    var querSubjectId = {};
    var queryIs_online = {};
    var queryYear = {};
    if (name !== undefined) {
        queryName = { name: { $regex: name, $options: 'i' } };
    }
    if (subject_id !== undefined) {
        querSubjectId = { subject_id: subject_id };
    }
    if (is_online !== undefined) {
        queryIs_online = { is_online: is_online };
    }
    if (Number.isInteger(year)) {
        queryYear = { "year": year };
    }
    var query = {
        $and:
            [
                queryName,
                querSubjectId,
                queryIs_online,
                queryYear
            ]
    };
    if (request.query.id !== undefined) {
        var id = [];
        if (Array.isArray(request.query.id)) {
            id = request.query.id;
        } else {
            id.push(request.query.id);
        }
        var queryIds = [];
        id.forEach((element) => {
            queryIds.push({ id: parseInt(element) });
        });
        query = { $or: queryIds };
    }
    try {
        if (is_exists === true) {
            var queryIds = [];
            var systemRooms = await systemRoomCollection.find().toArray();
            systemRooms.forEach(function (item) {
                queryIds.push({ id: { $ne: parseInt(item.exam_id) } });
            });
            queryIds.push(querSubjectId);
            queryIds.push(queryIs_online);
            query = {
                $and:
                    queryIds
            };
        }
        var count = await examCollection.find(query).toArray();
        var exams = [];
        if (is_tested !== undefined) {
            exams = await examCollection.find(query).toArray();
        } else {
            exams = await examCollection.find(query)
                .skip(queryRequest.skip)
                .limit(queryRequest.limit)
                .sort(queryRequest.sort)
                .project({ _id: 0 })
                .toArray();
        }
        for (var item of exams) {
            var is_tested_item = false;
            var is_rated_item = false;
            if (user_id !== undefined) {
                var tested_user = item.tested_user;
                if (tested_user.indexOf(user_id) !== -1) {
                    is_tested_item = true;
                }
                var rated_user = item.rated_user;
                if (rated_user.indexOf(user_id) !== -1) {
                    is_rated_item = true;
                }
            }
            var queryIs_tested = { is_tested: is_tested_item };
            Object.assign(item, queryIs_tested);
            var queryIs_rated = { is_rated: is_rated_item };
            Object.assign(item, queryIs_rated);
            delete item["tested_user"];
            delete item["rated_user"];
        }
        if (is_tested !== undefined) {
            var filtered = exams.filter(tested);
            if (is_tested !== true) {
                filtered = exams.filter(not_tested);
            }
            count = filtered;
            exams = paginate(filtered, queryRequest.limit, queryRequest.skip);
        }
        var arrayPromiseItem = [];
        for (var item of exams) {
            var promise = new Promise((resolve, reject) => {
                resolve(addArray(questionCollection, item));
            });
            arrayPromiseItem.push(promise);
        }
        Promise.all(arrayPromiseItem).then(values => {
            response.setHeader('X-Total-Count', count.length);
            response.status(200).json(values);
        });

    } catch (error) {
        response.sendStatus(error);
        console.log("get exam error : " + error);
    }
});

function tested(value) {
    return value.is_tested;
}

function not_tested(value) {
    return !value.is_tested;
}

function paginate(array, limit, skip) {
    return array.slice(skip, skip + limit);
}

async function addArray(questionCollection, item) {
    var array_answer = [];
    var array_thematic = [];
    var array_difficulty = [];
    var question = await questionCollection.find({ exam_id: item.id }).sort({ question: 1 }).toArray();
    question.forEach(element => {
        array_answer.push(element.key_answer);
        array_thematic.push(element.thematic_id);
        array_difficulty.push(element.difficulty);
    });
    var queryArray_answer = { array_answer: array_answer }
    Object.assign(item, queryArray_answer);
    var queryArray_thematic = { array_thematic: array_thematic }
    Object.assign(item, queryArray_thematic);
    var queryArray_difficulty = { array_difficulty: array_difficulty }
    Object.assign(item, queryArray_difficulty);
    return item;
}