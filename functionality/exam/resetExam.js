var exports = require('../../app');

exports.app.put("/resetExam", async (request, response) => {
    var examCollection = await exports.dbo.collection("Exam");
    var historyCollection = await exports.dbo.collection("History");
    var id = parseInt(request.query.id);
    var queryExam = { id: id };
    var queryHistory = { exam_id: id };
    var value = {
        is_online: true,
        count: 0,
        tested_user: [],
        rated_user: [],
        star: 0
    };
    try {
        var exam = await examCollection.findOne(queryExam);
        if (exam === null) {
            response.sendStatus(404);
        } else {
            var newValue = { $set: value };
            await examCollection.updateOne(queryExam, newValue);
            await historyCollection.deleteMany(queryHistory);
            response.status(200).json({});
        }
    } catch (error) {
        response.sendStatus(error);
        console.log("put : reset Exam error : " + error);
    }
});