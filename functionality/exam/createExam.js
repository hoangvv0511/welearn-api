var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var getRequest = require('../../config/getRequest').getRequest;

exports.app.post("/exam", checkToken, protectedRoute, checkRole, async (request, response) => {
    var examCollection = await exports.dbo.collection("Exam");
    var subjectCollection = await exports.dbo.collection("Subject");
    var thematicCollection = await exports.dbo.collection("Thematic");
    var questionCollection = await exports.dbo.collection("Question");
    queryRequest = getRequest(request);
    var name = request.body.name;
    var subject_id = request.body.subject_id;
    var question = request.body.question;
    var answer = request.body.answer;
    var is_onlineString = request.body.is_online;
    var year = parseInt(request.body.year);
    var start_id = parseInt(request.body.start_id);
    var exam_time = parseInt(request.body.exam_time);
    var number_of_question = parseInt(request.body.number_of_question);
    var is_online = false;
    if (is_onlineString === "true") {
        is_online = true
    }
    try {
        var querySubjectById = { id: subject_id };
        var subjectById = await subjectCollection.findOne(querySubjectById);
        var countSubject = 0;
        if (!Number.isInteger(number_of_question)) {
            if (subjectById !== null) {
                countSubject = subjectById.count;
            }
        } else {
            countSubject = number_of_question;
        }
        var queryThematicBySubjectId = { subject_id: subject_id };
        var thematicById = await thematicCollection.findOne(queryThematicBySubjectId);
        var thematic_id = "";
        if (thematicById !== null) {
            thematic_id = thematicById.id;
        }
        var numberQuestion = 0;
        var questions = await questionCollection.find().sort({ id: -1 }).toArray();
        if (questions.length !== 0) {
            numberQuestion = parseInt(questions[0].id);
        }
        var exams = await examCollection.find().sort({ id: -1 }).toArray();
        var countExam = 0;
        if (exams.length !== 0) {
            countExam = parseInt(exams[0].id);
        }
        var createQuestions = [];
        for (var i = 1; i <= countSubject; i++) {
            var createQuestion = {
                id: numberQuestion + i,
                exam_id: countExam + 1,
                key_answer: "A",
                thematic_id: thematic_id,
                difficulty: 0,
                question: start_id + i - 1
            };
            createQuestions.push(createQuestion);
        }
        newExam = {
            id: countExam + 1,
            name: name,
            subject_id: subject_id,
            question: question,
            answer: answer,
            exam_time: exam_time,
            number_of_question: countSubject,
            is_online: is_online,
            year: year,
            create_at: Date.now(),
            start_id: start_id,
            count: 0,
            tested_user: [],
            rated_user: [],
            star: 0
        };
        if (countSubject !== 0) {
            await questionCollection.insertMany(createQuestions);
        }
        await examCollection.insertOne(newExam);
        response.status(201).json(newExam);
    } catch (error) {
        response.sendStatus(error);
        console.log("post exam error : " + error);
    }
});