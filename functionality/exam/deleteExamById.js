var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.delete("/exam/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var examCollection = await exports.dbo.collection("Exam");
    var questionCollection = await exports.dbo.collection("Question");
    var systemRoomCollection = await exports.dbo.collection("SystemRoom");
    var historyCollection = await exports.dbo.collection("History");
    var id = parseInt(request.params.id);
    var queryDelete = { id: id };
    var queryDeleteQuestion = { exam_id: id };
    try {
        await systemRoomCollection.find(queryDeleteQuestion)
            .forEach(function (item) {
                item.exam_id = null;
                systemRoomCollection.save(item);
            });
        await examCollection.deleteOne(queryDelete);
        await questionCollection.deleteMany(queryDeleteQuestion);
        await historyCollection.deleteOne(queryDeleteQuestion);
        response.status(200).json({});
    } catch (error) {
        response.sendStatus(error);
        console.log("delete exam/id error : " + error);
    }
});