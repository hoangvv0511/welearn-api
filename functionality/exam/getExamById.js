var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.get("/exam/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var examCollection = await exports.dbo.collection("Exam");
    var questionCollection = await exports.dbo.collection("Question");
    var id = parseInt(request.params.id)
    var query = { id: id };
    var user_id = request.query.user_id;
    var is_tested = false;
    var is_rated = false;
    try {
        var exam = await examCollection.findOne(query);
        if (user_id !== undefined) {
            var tested_user = exam.tested_user;
            if(tested_user.indexOf(user_id) !== -1 ) {
                is_tested = true;
            }
            var rated_user = exam.rated_user;
            if(rated_user.indexOf(user_id) !== -1 ) {
                is_rated = true;
            }
        }
        var queryIs_tested = {is_tested : is_tested};
        Object.assign(exam, queryIs_tested);
        var queryIs_rated = {is_rated : is_rated};
        Object.assign(exam, queryIs_rated);
        var question = await questionCollection.find({exam_id : id}).sort({ question: 1 }).toArray();
        var array_answer = [];
        var array_thematic = [];
        var array_difficulty = [];
        question.forEach(element => {
            array_answer.push(element.key_answer);
            array_thematic.push(element.thematic_id);
            array_difficulty.push(element.difficulty);
        });
        var queryArray_answer = {array_answer : array_answer}
        Object.assign(exam, queryArray_answer);
        var queryArray_thematic = {array_thematic : array_thematic}
        Object.assign(exam, queryArray_thematic);
        var queryArray_difficulty = {array_difficulty : array_difficulty}
        Object.assign(exam, queryArray_difficulty);
        delete exam["_id"];
        delete exam["tested_user"];
        delete exam["rated_user"];
        if (exam === null) {
            response.setHeader('X-Total-Count', 0);
            response.status(404).json({});
        } else {
            response.setHeader('X-Total-Count', 1);
            response.status(200).json(exam);
        }
    } catch (error) {
        response.sendStatus(error);
        console.log("get exam/id error : " + error);
    }
});