var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.put("/exam/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var examCollection = await exports.dbo.collection("Exam");
    var questionCollection = await exports.dbo.collection("Question");
    var thematicCollection = await exports.dbo.collection("Thematic");
    var id = parseInt(request.params.id);
    var query = { id: id };
    var isOnlineString = request.body.is_online;
    var year = parseInt(request.body.year);
    var start_id = parseInt(request.body.start_id);
    var exam_time = parseInt(request.body.exam_time);
    var subject_id = request.body.subject_id;
    var is_online = false;
    if (isOnlineString === "true" || isOnlineString === true) {
        is_online = true
    }
    var value = {
        id: id,
        name: request.body.name,
        subject_id: subject_id,
        question: request.body.question,
        answer: request.body.answer,
        exam_time: exam_time,
        is_online: is_online,
        year: year,
        start_id: start_id
    };
    try {
        var newValue = { $set: value };
        var exam = await examCollection.findOne(query);
        if (exam === null) {
            response.sendStatus(404);
        } else {
            await examCollection.updateOne(query, newValue);
            response.status(200).json(value);
        }
        var queryThematicBySubjectId = { subject_id: subject_id };
        var thematicById = await thematicCollection.findOne(queryThematicBySubjectId);
        var thematic_id = "";
        if (thematicById !== null) {
            thematic_id = thematicById.id;
        }
        await questionCollection.find({exam_id : id}).forEach(function (item) {
            item.question = start_id;
            start_id++;
            item.thematic_id = thematic_id;
            questionCollection.save(item);
        });;

    } catch (error) {
        response.sendStatus(error);
        console.log("put exam/id error : " + error);
    }
});