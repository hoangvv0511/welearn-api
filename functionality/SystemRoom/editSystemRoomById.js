var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.put("/systemRoom/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var systemRoomCollection = await exports.dbo.collection("SystemRoom");
    var id = parseInt(request.params.id);
    var start_time = Date.parse(request.body.start_time);
    if (!Number.isInteger(start_time)) {
        start_time = parseFloat(request.body.start_time);
    }
    var query = { id: id };
    var value = {
        id: id,
        name: request.body.name,
        start_time: start_time,
        exam_id: request.body.exam_id
    };
    var newValue = { $set: value };
    var systemRoom = await systemRoomCollection.findOne(query);
    if (systemRoom === null) {
        response.sendStatus(404);
    } else {
        try {
            await systemRoomCollection.updateOne(query, newValue);
            response.status(200).json(value);
        } catch (error) {
            response.sendStatus(error);
            console.log("put systemRoom/id error : " + error);
        }
    }
});