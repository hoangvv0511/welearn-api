require('./getAllSystemRooms');
require('./getSystemRoomById');
require('./options');
require('./createSystemRoom');
require('./deleteSystemRoomById');
require('./deleteSystemRoomAfter3Day');
require('./editSystemRoomById');