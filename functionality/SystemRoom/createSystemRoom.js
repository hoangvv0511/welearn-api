var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var getRequest = require('../../config/getRequest').getRequest;

exports.app.post("/systemRoom", checkToken, protectedRoute, checkRole, async (request, response) => {
    var systemRoomCollection = await exports.dbo.collection("SystemRoom");
    queryRequest = getRequest(request);
    var name = request.body.name;
    var start_time = request.body.start_time;
    var exam_id = parseInt(request.body.exam_id);
    var start_time = Date.parse(request.body.start_time);
    if (!Number.isInteger(start_time)) {
        start_time = parseFloat(request.body.start_time);
    }
    try {
        var systemRooms = await systemRoomCollection.find().sort({ id: -1 }).toArray();
        var countsystemRoom = 0;
        if (systemRooms.length !== 0) {
            countsystemRoom = parseInt(systemRooms[0].id);
        }
        newSystemRoom = {
            id: countsystemRoom + 1,
            name: name,
            start_time: start_time,
            exam_id: exam_id,
            create_at: Date.now()
        };
        await systemRoomCollection.insertOne(newSystemRoom);
        response.status(201).json(newSystemRoom);
    } catch (error) {
        response.sendStatus(error);
        console.log("post systemRoom error : " + error);
    }
});