var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.delete("/systemRoom/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var systemRoomCollection = await exports.dbo.collection("SystemRoom");
    var id = parseInt(request.params.id);
    var query = { id: id };
    try {
        await systemRoomCollection.deleteOne(query);
        response.status(200).json({});
    } catch (error) {
        response.sendStatus(error);
        console.log("delete systemRoom/id error : " + error);
    }
});