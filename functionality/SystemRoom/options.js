var exports = require('../../app');

exports.app.options("/systemRoom/:id", (request, response) => {
    response.sendStatus(204);
});

exports.app.options("/systemRoom", (request, response) => {
    response.sendStatus(204);
});