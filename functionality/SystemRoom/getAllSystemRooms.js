var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var getRequest = require('../../config/getRequest').getRequest;

exports.app.get("/systemRoom", checkToken, protectedRoute, checkRole, async (request, response) => {
    var systemRoomCollection = await exports.dbo.collection("SystemRoom");
    var examCollection = await exports.dbo.collection("Exam");
    queryRequest = getRequest(request);
    var id = parseInt(request.query.id);
    var subject_id = request.query.subject_id;
    var name = request.query.name;
    var year = parseInt(request.query.year);
    var is_open = request.query.is_open;
    var queryId = {};
    var querySubjectId = {};
    var queryName = {};
    var queryYear = {};
    if (Number.isInteger(id)) {
        queryId = { id: id };
    }
    if (subject_id !== undefined) {
        querySubjectId = { "systemRoom_exam.subject_id": subject_id };
    }
    if (name !== undefined) {
        queryName = { name: { $regex: name, $options: 'i' } };
    }
    if (Number.isInteger(year)) {
        queryYear = { "systemRoom_exam.year": year };
    }
    var match = {
        $and:
            [
                queryId,
                querySubjectId,
                queryName,
                queryYear
            ]
    }
    var query = [
        {
            $lookup:
            {
                from: 'Exam',
                localField: 'exam_id',
                foreignField: 'id',
                as: 'systemRoom_exam'
            }
        },
        {
            $unwind: {
                path: "$systemRoom_exam",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $match: match
        }
    ]
    try {
        var count = await systemRoomCollection.aggregate(query).toArray();
        var systemRooms = [];
        var project = {
            _id: 0,
            "systemRoom_exam._id": 0,
            "systemRoom_exam.id": 0,
            "systemRoom_exam.question": 0,
            "systemRoom_exam.answer": 0,
            "systemRoom_exam.is_online": 0,
            "systemRoom_exam.year": 0,
            "systemRoom_exam.start_id": 0,
            "systemRoom_exam.create_at": 0,
            "systemRoom_exam.count": 0,
            "systemRoom_exam.tested_user": 0,
            "systemRoom_exam.rated_user": 0,
            "systemRoom_exam.star": 0
        }
        if ((queryRequest.skip === 0 && queryRequest.limit === 0) || is_open !== undefined) {
            systemRooms = await systemRoomCollection.aggregate(query).project(project).toArray();
        } else {
            systemRooms = await systemRoomCollection.aggregate(query)
                .skip(queryRequest.skip)
                .limit(queryRequest.limit)
                .sort(queryRequest.sort)
                .project(project)
                .toArray();
        }
        if (is_open !== undefined) {
            if (is_open === "not_open") {
                var filtered = systemRooms.filter(not_open);
                count = filtered;
                systemRooms = paginate(filtered, queryRequest.limit, queryRequest.skip);
            }
            if (is_open === "openning") {
                var filtered = systemRooms.filter(openning);
                count = filtered;
                systemRooms = paginate(filtered, queryRequest.limit, queryRequest.skip);
            }
        }
        response.setHeader('X-Total-Count', count.length);
        response.status(200).json(systemRooms);
    } catch (error) {
        response.sendStatus(error);
        console.log("get systemRoom error : " + error);
    }
});

function paginate(array, limit, skip) {
    return array.slice(skip, skip + limit);
}

function not_open(value) {
    var DateNow = Date.now();
    if (DateNow < value.start_time) {
        return true;
    }
    return false;
}

function openning(value) {
    var DateNow = Date.now();
    var end_time = value.start_time + value.systemRoom_exam.exam_time * 60 * 1000;
    if (DateNow >= value.start_time && DateNow <= end_time) {
        return true;
    }
    return false;
}