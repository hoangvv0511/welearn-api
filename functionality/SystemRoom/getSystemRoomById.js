var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.get("/systemRoom/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var systemRoomCollection = await exports.dbo.collection("SystemRoom");
    var examCollection = await exports.dbo.collection("Exam");
    var questionCollection = await exports.dbo.collection("Question");
    var id = parseInt(request.params.id);
    var queryId = { id: id };
    var user_id = request.query.user_id;
    var is_tested = false;
    var is_rated = false;
    var match = {
        $and:
            [
                queryId
            ]
    }
    var query = [
        {
            $lookup:
            {
                from: 'Exam',
                localField: 'exam_id',
                foreignField: 'id',
                as: 'systemRoom_exam'
            }
        },
        {
            $unwind: {
                path: "$systemRoom_exam",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $match: match
        }
    ]
    try {
        var project = {
            _id: 0,
            "systemRoom_exam._id": 0,
            "systemRoom_exam.tested_user": 0,
            "systemRoom_exam.rated_user": 0
        }
        var systemRoom = await systemRoomCollection.aggregate(query).project(project).toArray();
        if (systemRoom[0] === undefined) {
            response.setHeader('X-Total-Count', 0);
            response.status(404).json({});
            return;
        }
        systemRoom = systemRoom[0];
        var query = { id: systemRoom.exam_id }
        var exam = await examCollection.findOne(query);
        if (user_id !== undefined) {
            var tested_user = exam.tested_user;
            if (tested_user.indexOf(user_id) !== -1) {
                is_tested = true;
            }
            var rated_user = exam.rated_user;
            if (rated_user.indexOf(user_id) !== -1) {
                is_rated = true;
            }
        }
        var queryIs_tested = { is_tested: is_tested };
        Object.assign(systemRoom, queryIs_tested);
        var queryIs_rated = { is_rated: is_rated };
        Object.assign(systemRoom, queryIs_rated);
        var question = await questionCollection.find({exam_id : systemRoom.systemRoom_exam.id}).sort({ question: 1 }).toArray();
        var array_answer = [];
        var array_thematic = [];
        var array_difficulty = [];
        question.forEach(element => {
            array_answer.push(element.key_answer);
            array_thematic.push(element.thematic_id);
            array_difficulty.push(element.difficulty);
        });
        var queryArray_answer = {array_answer : array_answer}
        Object.assign(systemRoom, queryArray_answer);
        var queryArray_thematic = {array_thematic : array_thematic}
        Object.assign(systemRoom, queryArray_thematic);
        var queryArray_difficulty = {array_difficulty : array_difficulty}
        Object.assign(systemRoom, queryArray_difficulty);
        if (systemRoom === null) {
            response.setHeader('X-Total-Count', 0);
            response.status(404).json({});
        } else {
            response.setHeader('X-Total-Count', 1);
            response.status(200).json(systemRoom);
        }
    } catch (error) {
        response.sendStatus(error);
        console.log("get systemRoom/id error : " + error);
    }
});