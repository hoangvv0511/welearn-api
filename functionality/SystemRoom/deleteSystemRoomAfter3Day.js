var exports = require('../../app');
var lifeSystemRoom = require('../../config/constants').lifeSystemRoom;
var cron = require("node-cron");

var task = cron.schedule('0 0 */6 * * *', async () => {
    console.log('Bắt đầu tiến trình xóa phòng thi online')
    var systemRoomCollection = await exports.dbo.collection("SystemRoom");
    var examCollection = await exports.dbo.collection("Exam");
    try {
        var systemRooms = await systemRoomCollection.find().toArray();
        var dateNow = Date.now();
        systemRooms.map(async function (item) {
            if (dateNow - item.create_at >= lifeSystemRoom) {
                console.log(`Đang xóa phòng thi #${item.id}`)
                await systemRoomCollection.deleteOne(item);
                var query = { id: item.exam_id };
                var value = {
                    is_online: false
                };
                var newValue = { $set: value };
                await examCollection.updateOne(query, newValue);
                console.log(`Đã chuyển đề #${item.exam_id} sang offline`)
            }
        });
    } catch (error) {
        console.log("delete systemRoom after 3 day error : " + error);
    }
});
