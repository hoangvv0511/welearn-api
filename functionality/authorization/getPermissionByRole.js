var exports = require('../..');

var getPermissionByRole = async (role) => {
    var authorizationCollection = await exports.dbo.collection("Authorization");
    var query = { role: role };
    var authorization = await authorizationCollection.findOne(query);
    return authorization.permissions;
}

module.exports = {
    getPermissionByRole: getPermissionByRole
}