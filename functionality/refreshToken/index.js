var exports = require('../..');
var jwt = require('jsonwebtoken');
var secretOrKey = require('../../config/constants').secretOrKey;
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var lifeToken = require('../../config/constants').lifeToken;

exports.app.get("/refreshToken", checkToken, protectedRoute, async (request, response) => {
    return;
    var role = request.user.role;
    var username = request.user.username;
    var refreshToken = jwt.sign({ username: username, role: role }, secretOrKey, { expiresIn: lifeToken }); // seconds
    response.status(200).json({ username: username, role: role, refreshToken: refreshToken });
});