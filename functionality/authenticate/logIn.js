var exports = require('../..');
var getUserByUsername = require('./getUserByUsername').getUserByUsername;
var jwt = require('jsonwebtoken');
var secretOrKey = require('../../config/constants').secretOrKey;
var lifeToken = require('../../config/constants').lifeToken;

exports.app.post("/authenticate", async (request, response) => {
    var username = request.body.username;
    var password = request.body.password;
    var user = await getUserByUsername(username);
    if (user === null) {
        response.statusMessage = "ra.auth.username_not_exists";
        response.status(400).end();
    } else {
        if (user.password === password) {
            var role = user.role;
            var token = jwt.sign({ username: username, role: role }, secretOrKey, { expiresIn: lifeToken }); // seconds
            response.status(200).json({ username: username, role: role, token: token });
        } else {
            response.statusMessage = "ra.auth.password_incorrect";
            response.status(400).end();
        }
    }
});