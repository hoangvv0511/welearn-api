var exports = require('../..');

var getUserByUsername = async (username) => {
    var authenticateCollection = await exports.dbo.collection("Authenticate");
    var query = { username: username };
    var user = await authenticateCollection.findOne(query);
    return user;
}

module.exports = {
    getUserByUsername: getUserByUsername
}