var exports = require('../..');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.put("/user/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var userCollection = await exports.dbo.collection("User");
    var id = request.params.id;
    var query = { id: id };
    var value = {
        id: id,
        image: request.body.image,
        name: request.body.name,
    };
    var newValue = { $set: value };
    var user = await userCollection.findOne(query);
    if (user === null) {
        response.sendStatus(404);
    } else {
        try {
            await userCollection.updateOne(query, newValue);
            var userAfterUpdate = await userCollection.findOne(query);
            delete userAfterUpdate['_id'];
            response.status(200).json(userAfterUpdate);
        } catch (error) {
            response.sendStatus(error);
            console.log("put user/id error : " + error);
        }
    }
});