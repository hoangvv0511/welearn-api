var exports = require('../..');

exports.app.options("/user/:id", (request, response) => {
    response.sendStatus(204);
});

exports.app.options("/user", (request, response) => {
    response.sendStatus(204);
});