var exports = require('../..');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var getRequest = require('../../config/getRequest').getRequest;

exports.app.get("/user", checkToken, protectedRoute, checkRole, async (request, response) => {
    var userCollection = await exports.dbo.collection("User");
    queryRequest = getRequest(request);
    var q = request.query.q;
    var query = {};
    if (q !== undefined) {
        query = {
            $and:
                [
                    { name: { $regex: q, $options: 'i' } },
                    { email: { $regex: q, $options: 'i' } }
                ]
        };
    }
    try {
        var count = await userCollection.aggregate(query).toArray();
        var users = [];
        if (queryRequest.skip === 0 && queryRequest.limit === 0) {
            users = await userCollection.aggregate(query).project({ _id: 0 }).toArray();
        } else {
            users = await userCollection.aggregate(query)
                .skip(queryRequest.skip)
                .limit(queryRequest.limit)
                .sort(queryRequest.sort)
                .project({ _id: 0 })
                .toArray();
        }
        response.setHeader('X-Total-Count', count.length);
        response.status(200).json(users);
    } catch (error) {
        response.sendStatus(error);
        console.log("get user error : " + error);
    }
});