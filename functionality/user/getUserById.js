var exports = require('../..');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.get("/user/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var userCollection = await exports.dbo.collection("User");
    var query = { id: request.params.id };
    try {
        var user = await userCollection.findOne(query);
        if (user === null) {
            response.setHeader('X-Total-Count', 0);
            response.status(404).json({});
        } else {
            response.setHeader('X-Total-Count', 1);
            delete user['_id'];
            response.status(200).json(user);
        }
    } catch (error) {
        response.sendStatus(error);
        console.log("get user/id error : " + error);
    }
});