var exports = require('../..');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.delete("/user/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var userCollection = await exports.dbo.collection("User");
    var query = { id: request.params.id };
    try {
        await userCollection.deleteOne(query);
        response.status(200).json({});
    } catch (error) {
        response.sendStatus(error);
        console.log("delete user/id error : " + error);
    }
});