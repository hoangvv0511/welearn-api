var exports = require('../..');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.post("/checkUserExists", checkToken, protectedRoute, checkRole, async (request, response) => {
    var userCollection = await exports.dbo.collection("User");
    var query = {id: request.body.id};
    try {
        var user = await userCollection.find(query).project({ _id: 0 }).toArray();
        if (user !== null) {
            response.status(200).json(user[0]);
        } else {
            response.status(200).json({});
        }
    } catch (error) {
        response.sendStatus(error);
        console.log("post check User Exists error : " + error);
    }
});