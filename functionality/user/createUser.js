var exports = require('../..');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.post("/user", checkToken, protectedRoute, checkRole, async (request, response) => {
    var userCollection = await exports.dbo.collection("User");
    try {
        newUser = {
            id: request.body.id,
            image: request.body.image,
            name: request.body.name,
            email: request.body.email,
        };
        await userCollection.insertOne(newUser);
        response.status(201).json(newUser);
    } catch (error) {
        response.sendStatus(error);
        console.log("post user error : " + error);
    }
});