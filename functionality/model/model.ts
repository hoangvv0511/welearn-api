class Thematic { // chuyên đề
    id: string // ham_so, mu_logarit
    name: string // Hàm số, Mũ-Logarit
    subject_id: string // toan, ly, hoa
}

class Subject { // môn học
    id: string // toan, ly, hoa,...
    name: string // Toán, Lý, Hóa,...
    // thematic : Array<Thematic> //lúc gọi API trả về mảng chuyên đề
}

class Question { // câu hỏi
    id: number // tăng dần 
    exam_id: number // tham chiếu đến Exam id
    key_answer: string // đáp án A B C D
    thematic_id: string //  tham chiếu đến Thematic Slug để truy vấn
    // thematic : Thematic // lúc gọi API trả về chuyên đề : slug, name
    difficulty: number // (0-1-2-3-4) :(default, nhớ, hiểu, vận dụng, vận dụng cao)
}

class Exam { // đề thi
    id: number // Exam Id
    name: string // tên Exam
    subject_id: string // tham chiếu đến Subject Slug
    // subject : Subject // lúc gọi API trả về Subject
    question: string // link driver đề thi
    anwser: string // link driver đáp án
    // list_question : Array<Question> // lúc gọi API trả về mảng câu hỏi với Id Exam
    number_question: number // số lượng câu hỏi
    time: number // thời gian làm bài tính theo phút
    is_online: boolean
    year : number // năm của đề
    create_at : Date // thời gian tạo đề
    start_id : number // câu bắt đầu

    count : number // mỗi lần gọi history tăng lên 1
    tested_user : [] // mảng user đã thi
    rated_user : [] // mảng user đã đánh giá
    star : number // tổng số sao đánh giá
}

class Histori { //History
    id: number
    point: number // decimal : 2 chữ số thập phân 9.25
    start_time: Date // thời gian làm xong đề thi
    end_time: Date // thời gian làm bài, có thể làm xong xong sớm hơn thời gian đề thi
    user_id: string // user id
    exam_id: number // exam id
}

class User {
    id: number
    image: string
    name: string
    email:string
    // history: Arg id của ray<number> // mảnHistory request trả về
    type : number // 0: gmail, 1: facebook
}

class SystemRoom {
    id: number
    name: string
    start_time: Date
    exam_time: Date
    subject_id: string
    exam_id: number
    users: Array<User> // array uid
}

class UserRoom {
    id: number
    name: string
    start_time: Date
    exam_time: number
    subject_id: string
    exam_id: number
    user: Array<User> // array uid
}