var exports = require('../..');

exports.app.options("/subject/:id", (request, response) => {
    response.sendStatus(204);
});

exports.app.options("/subject", (request, response) => {
    response.sendStatus(204);
});