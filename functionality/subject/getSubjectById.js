var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.get("/subject/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var subjectCollection = await exports.dbo.collection("Subject");
    var query = { id: request.params.id };
    try {
        var subject = await subjectCollection.findOne(query);
        if (subject === null) {
            response.setHeader('X-Total-Count', 0);
            response.status(404).json({});
        } else {
            response.setHeader('X-Total-Count', 1);
            response.status(200).json(subject);
        }
    } catch (error) {
        response.sendStatus(error);
        console.log("get subject/id error : " + error);
    }
});