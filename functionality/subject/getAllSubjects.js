var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var getRequest = require('../../config/getRequest').getRequest;

exports.app.get("/subject", checkToken, protectedRoute, checkRole, async (request, response) => {
    var subjectCollection = await exports.dbo.collection("Subject");
    queryRequest = getRequest(request);
    var name = request.query.name;
    var query = {};
    if (name !== undefined) {
        query = {
            $and:
                [
                    { name: { $regex: name, $options: 'i' } },
                ]
        };
    }
    try {
        var count = await subjectCollection.find(query).toArray();
        var subjects = await subjectCollection.find(query)
            .project({ _id: 0 })
            .toArray();
        response.setHeader('X-Total-Count', count.length);
        subjects.forEach((element, index) => {
            var STT = { STT: index + queryRequest.skip + 1 };
            Object.assign(element, STT);
        });
        response.status(200).json(subjects);
    } catch (error) {
        response.sendStatus(error);
        console.log("get subject error : " + error);
    }
});