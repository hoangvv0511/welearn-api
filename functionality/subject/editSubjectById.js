var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.put("/subject/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var subjectCollection = await exports.dbo.collection("Subject");
    var id = request.params.id;
    var query = { id: id };
    var value = {
        id: id,
        name: request.body.name
    };
    var newValue = { $set: value };
    var subject = await subjectCollection.findOne(query);
    if (subject === null) {
        response.sendStatus(404);
    } else {
        try {
            await subjectCollection.updateOne(query, newValue);
            response.status(200).json(value);
        } catch (error) {
            response.sendStatus(error);
            console.log("put subject/id error : " + error);
        }
    }
});