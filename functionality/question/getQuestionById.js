var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.get("/question/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var questionCollection = await exports.dbo.collection("Question");
    var query = { id: parseInt(request.params.id) };
    try {
        var question = await questionCollection.findOne(query);
        if (question === null) {
            response.setHeader('X-Total-Count', 0);
            response.status(404).json({});
        } else {
            response.setHeader('X-Total-Count', 1);
            response.status(200).json(question);
        }
    } catch (error) {
        response.sendStatus(error);
        console.log("get question/id error : " + error);
    }
});