var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var getRequest = require('../../config/getRequest').getRequest;

exports.app.get("/question", checkToken, protectedRoute, checkRole, async (request, response) => {
    var questionCollection = await exports.dbo.collection("Question");
    queryRequest = getRequest(request);
    var exam_id = parseInt(request.query.exam_id);
    var subject_id = request.query.subject_id;
    var queryExamId = {};
    var querySubjectId = {};
    if (Number.isInteger(exam_id)) {
        queryExamId = { exam_id: exam_id };
    }
    if (subject_id !== undefined) {
        querySubjectId = { "thematic_subject.id": subject_id };
    }
    var match = {
        $and:
            [
                queryExamId,
                querySubjectId
            ]
    };
    var query = [
        {
            $lookup:
            {
                from: 'Thematic',
                localField: 'thematic_id',
                foreignField: 'id',
                as: 'question_thematic'
            }
        },
        {
            $unwind: {
                path: "$question_thematic",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup:
            {
                from: 'Subject',
                localField: 'question_thematic.subject_id',
                foreignField: 'id',
                as: 'thematic_subject'
            }
        },
        {
            $unwind: {
                path: "$thematic_subject",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $match: match
        }
    ]
    try {
        var count = await questionCollection.aggregate(query).toArray();
        var questions = [];
        if (queryRequest.skip === 0 && queryRequest.limit === 0) {
            questions = await questionCollection.aggregate(query).project({ _id: 0 }).toArray();
        } else {
            questions = await questionCollection.aggregate(query)
                .skip(queryRequest.skip)
                .limit(queryRequest.limit)
                .sort(queryRequest.sort)
                .project({ _id: 0 })
                .toArray();
        }
        response.setHeader('X-Total-Count', count.length);
        response.status(200).json(questions);
    } catch (error) {
        response.sendStatus(error);
        console.log("get question error : " + error);
    }
});