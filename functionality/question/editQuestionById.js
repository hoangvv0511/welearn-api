var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;

exports.app.put("/question/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var questionCollection = await exports.dbo.collection("Question");
    var id = parseInt(request.params.id);
    var query = { id: id };
    var value = {
        id: id,
        exam_id: request.body.exam_id,
        key_answer: request.body.key_answer,
        thematic_id: request.body.thematic_id,
        difficulty: request.body.difficulty
    };
    var newValue = { $set: value };
    var question = await questionCollection.findOne(query);
    if (question === null) {
        response.sendStatus(404);
    } else {
        try {
            await questionCollection.updateOne(query, newValue);
            response.status(200).json(value);
        } catch (error) {
            response.sendStatus(error);
            console.log("put question/id error : " + error);
        }
    }
});