var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var updateNumberOfQuestion = require('./lib').updateNumberOfQuestion;

exports.app.delete("/question/:id", checkToken, protectedRoute, checkRole, async (request, response) => {
    var questionCollection = await exports.dbo.collection("Question");
    var id = parseInt(request.params.id);
    var query = { id: id };
    try {
        var question = await questionCollection.findOne(query);
        var exam_id = question.exam_id;
        var count = 1;
        await questionCollection.deleteOne(query);
        await questionCollection.find({ exam_id: exam_id })
            .forEach(function (item) {
                item.question = count;
                count++;
                questionCollection.save(item);
            });
        await updateNumberOfQuestion(exam_id);
        response.status(200).json({});
    } catch (error) {
        response.sendStatus(error);
        console.log("delete question/id error : " + error);
    }
});