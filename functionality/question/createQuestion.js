var exports = require('../../app');
var checkToken = require('../../config/checkToken').checkToken;
var protectedRoute = require('../../config/protectedRoute').protectedRoute;
var checkRole = require('../../config/checkRole').checkRole;
var getNumberQuestionByExamId = require('./lib').getNumberQuestionByExamId;
var updateNumberOfQuestion = require('./lib').updateNumberOfQuestion;

exports.app.post("/question", checkToken, protectedRoute, checkRole, async (request, response) => {
    var questionCollection = await exports.dbo.collection("Question");
    var exam_id = parseInt(request.body.exam_id);
    var thematic_id = request.body.thematic_id;
    var key_answer = request.body.key_answer;
    var difficulty = parseInt(request.body.difficulty);
    var question = await getNumberQuestionByExamId(exam_id);
    try {
        var count = await questionCollection.find().toArray();
        newQuestion = {
            id: count.length + 1,
            exam_id: exam_id,
            key_answer: key_answer,
            thematic_id : thematic_id,
            difficulty : difficulty,
            question : question
        };
        await questionCollection.insertOne(newQuestion);
        await updateNumberOfQuestion(exam_id);
        response.status(201).json(newQuestion);
    } catch (error) {
        response.sendStatus(error);
        console.log("post thematic error : " + error);
    }
});