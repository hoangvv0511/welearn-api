var exports = require('../../app');

var getNumberQuestionByExamId = async (exam_id) => {
    var questionCollection = await exports.dbo.collection("Question");
    var query = { exam_id: exam_id };
    try {
        if (!Number.isInteger(exam_id)) {
            return 0;
        } else {
            var questions = await questionCollection.find(query).sort({ question: -1 }).toArray();
            var question = parseInt(questions[0].question) + 1;
            return question;
        }
    } catch (error) {
        response.sendStatus(error);
        console.log("question lib error : " + error);
    }
}

var updateNumberOfQuestion = async (exam_id) => {
    var questionCollection = await exports.dbo.collection("Question");
    var examCollection = await exports.dbo.collection("Exam");
    var query = { id: exam_id };
    try {
        var questions = await questionCollection.find({ exam_id: exam_id }).toArray();
        var number_of_question = questions.length;
        var value = {
            number_of_question: number_of_question
        };
        var newValue = { $set: value };
        await examCollection.updateOne(query, newValue);
    } catch (error) {
        response.sendStatus(error);
        console.log("question lib error : " + error);
    }
}

module.exports = {
    getNumberQuestionByExamId: getNumberQuestionByExamId,
    updateNumberOfQuestion: updateNumberOfQuestion
}