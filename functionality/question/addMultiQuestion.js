var exports = require('../../app');

exports.app.post("/addMultiQuestion", async (request, response) => {
    var questionCollection = await exports.dbo.collection("Question");
    var thematicCollection = await exports.dbo.collection("Thematic");
    var exam_id = parseInt(request.body.exam_id);
    var key_answers = request.body.key_answers;
    var subject_id = request.body.subject_id;
    if (key_answers !== undefined) {
        var array_key_answers = key_answers.split('');
    }
    try {
        var randomThematic = await thematicCollection.find({ subject_id: subject_id }).toArray();
        var questions = await questionCollection.find({ exam_id: exam_id }).sort({ quesiton: 1 }).toArray();
        questions.forEach((element, index) => {
            if (key_answers !== undefined) {
                switch (array_key_answers[index]) {
                    case "1":
                        element.key_answer = "A";
                        break;
                    case "2":
                        element.key_answer = "B";
                        break;
                    case "3":
                        element.key_answer = "C";
                        break;
                    case "4":
                        element.key_answer = "D";
                        break;
                    default:
                        element.key_answer = "D";
                }
            }
            if (subject_id !== undefined) {
                var thematic = getRandomInt(0, randomThematic.length);
                element.thematic_id = randomThematic[thematic].id;
            }
            element.difficulty = getRandomInt(1, 5);
            questionCollection.save(element);
        });
        response.json();
    } catch (error) {
        response.sendStatus(error);
        console.log("get addMultiQuestion error : " + error);
    }
});

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}