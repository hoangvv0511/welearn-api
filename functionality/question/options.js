var exports = require('../..');

exports.app.options("/question/:id", (request, response) => {
    response.sendStatus(204);
});

exports.app.options("/question", (request, response) => {
    response.sendStatus(204);
});